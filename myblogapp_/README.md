# Sample GitLab Project

This sample project shows how a project in GitLab looks for demonstration purposes. It contains issues, merge requests and Markdown files in many branches,
named and filled with lorem ipsum.

You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)


#実行環境
```
(py36) tera-07@TERA-07noMacBook-Pro myblogapp_ % python -m django --version
3.2.5
(py36) tera-07@TERA-07noMacBook-Pro myblogapp_ % python --version
Python 3.9.7
```
# 起動方法
$ python manage.py runserver
